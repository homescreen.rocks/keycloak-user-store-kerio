package rocks.homescreen.keycloakuserstorekerio.keriohttpclient;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.keycloak.models.GroupModel;
import rocks.homescreen.keycloakuserstorekerio.exceptions.KerioClientErrorResponse;
import rocks.homescreen.keycloakuserstorekerio.models.Group;
import rocks.homescreen.keycloakuserstorekerio.models.User;
import rocks.homescreen.keycloakuserstorekerio.models.requests.*;

import java.net.MalformedURLException;
import java.util.*;

public class KerioHttpClient extends AbstractHttpClient {
    private final String applicationName;
    private final String applicationVendor;
    private final String applicationVersion;
    private final String kerioUsername;
    private final String kerioPassword;
    private final String kerioDefaultGroup;


    public KerioHttpClient(String serverAddress, String applicationName, String username, String password, String defaultGroup) throws MalformedURLException {
        super(serverAddress, "/admin/api/jsonrpc");
        this.applicationName = applicationName;
        this.applicationVendor = "homescreen.rocks";
        this.applicationVersion = "1.0.0";

        this.kerioUsername = username;
        this.kerioPassword = password;
        this.kerioDefaultGroup = defaultGroup;
    }

    public void login() throws JSONRPC2SessionException, KerioClientErrorResponse {
        var on = JsonNodeFactory.instance.objectNode();
        on
                .put("userName", this.kerioUsername)
                .put("password", this.kerioPassword)
                .with("application")
                .put("name", this.applicationName)
                .put("vendor", this.applicationVendor)
                .put("version", this.applicationVersion);

        JSONObject response = null;

        response = this.sendJsonRpc2Request("Session.login", on);
        System.out.println(response.get("token"));
        var token = response.get("token").toString();
        var headers = new HashMap<String, String>();
        headers.put("X-Token", token);
        this.setCustomHeaders(headers);
    }

    public void logout() throws JSONRPC2SessionException, KerioClientErrorResponse {
        var on = JsonNodeFactory.instance.objectNode();
        this.post("Session.logout", on);
    }

    public Optional<User> createUser(String newUsername) {
        try {
            // 1. create user
            var request = new CreateUserRequest();
            var newUser = new User();
            newUser.setEmail(newUsername);
            newUser.getCredentials().setUserName(newUsername);

            if (!StringUtils.isBlank(kerioDefaultGroup)) {
                var group = this.findGroupByString(kerioDefaultGroup, 0, 1).stream().findFirst();
                if (group.isPresent()) {
                    var userGroup = new Group();
                    userGroup.setId(group.get().getId());
                    newUser.getGroups().add(userGroup);
                }
            }

            request.getUsers().add(newUser);
            var data = this.post("Users.create", request);

            Gson gson = new GsonBuilder().create();
            String userId = ((JSONObject) ((JSONArray) data.get("result")).get(0)).get("id").toString();
            newUser.setId(UUID.fromString(userId));

            return Optional.of(newUser);

        } catch (JSONRPC2SessionException | KerioClientErrorResponse e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public JSONObject getConfigTimestamp() {
        try {
            var data = this.post("Session.getConfigTimestamp", null);
            return (JSONObject) data;
        } catch (JSONRPC2SessionException | KerioClientErrorResponse e) {
            e.printStackTrace();
        }
        return null;
    }

    public void confirmConfig(JSONObject sessionTimestamp) {
        try {
            var data = this.post("Session.confirmConfig", sessionTimestamp);
        } catch (JSONRPC2SessionException | KerioClientErrorResponse e) {
            e.printStackTrace();
        }
    }

    public Optional<User> findUserByUsername(String username) {
        var request = new FindUserRequest();
        try {
            request.getQuery().setLimit(1);

            var filter = new FilterCondition("userName", FilterComparator.Eq, username);

            request.getQuery().getConditions().add(filter);

            JSONObject data = this.post("Users.get", request);
            Gson gson = new GsonBuilder().create();
            List<User> user = gson.fromJson(data.get("list").toString(), new TypeToken<List<User>>() {
            }.getType());

            return user.stream().findFirst();

        } catch (JSONRPC2SessionException | KerioClientErrorResponse e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public Optional<User> findUserById(String id) {
        var request = new FindUserRequest();
        try {
            request.getQuery().setLimit(1);

            var filter = new FilterCondition("id", FilterComparator.Eq, id);

            request.getQuery().getConditions().add(filter);

            var data = this.post("Users.get", request);

            Gson gson = new GsonBuilder().create();
            List<User> user = gson.fromJson(data.get("list").toString(), new TypeToken<List<User>>() {
            }.getType());

            return user.stream().findFirst();

        } catch (JSONRPC2SessionException | KerioClientErrorResponse e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public Optional<User> findUserByEmail(String email) {
        return findUserByEmail(email, null, null);
    }

    public Optional<User> findUserByEmail(String email, Integer start, Integer limit) {

        var request = new FindUserRequest();
        try {
            request.getQuery().setLimit(1);

            var filter = new FilterCondition("email", FilterComparator.Eq, email);

            request.getQuery().getConditions().add(filter);

            if (start != null) {
                request.getQuery().setStart(start);
            }
            if (limit != null) {
                request.getQuery().setLimit(limit);
            }

            var data = this.post("Users.get", request);

            Gson gson = new GsonBuilder().create();
            List<User> user = gson.fromJson(data.get("list").toString(), new TypeToken<List<User>>() {
            }.getType());

            return user.stream().findFirst();

        } catch (JSONRPC2SessionException | KerioClientErrorResponse e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public Optional<User> findUserByString(String search) {
        return findUserByString(search, null, null);
    }

    public Optional<User> findUserByString(String search, Integer start, Integer limit) {

        var request = new FindUserRequest();
        try {
            request.getQuery().setLimit(1);

            var filterUsername = new FilterCondition("loginName", FilterComparator.Like, search);
            var filterFullName = new FilterCondition("fullName", FilterComparator.Like, search);

            request.getQuery().getConditions().add(filterUsername);
            request.getQuery().setCombining("Or");
            request.getQuery().getConditions().add(filterFullName);

            if (start != null) {
                request.getQuery().setStart(start);
            }
            if (limit != null) {
                request.getQuery().setLimit(limit);
            }

            var data = this.post("Users.get", request);

            Gson gson = new GsonBuilder().create();
            List<User> user = gson.fromJson(data.get("list").toString(), new TypeToken<List<User>>() {
            }.getType());

            return user.stream().findFirst();

        } catch (JSONRPC2SessionException | KerioClientErrorResponse e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public List<User> findUsersByParams(Map<String, String> params, Integer start, Integer limit) {

        var request = new FindUserRequest();
        try {
            request.getQuery().setLimit(limit);

            params.forEach((key, value) -> {
                switch (key) {
                    case "email":
                        request.getQuery().getConditions().add(new FilterCondition("email", FilterComparator.Like, value));
                        break;
                    case "firstName":
                    case "lastName":
                        request.getQuery().getConditions().add(new FilterCondition("fullName", FilterComparator.Like, value));
                        break;
                    case "username":
                        request.getQuery().getConditions().add(new FilterCondition("loginName", FilterComparator.Like, value));
                        break;
                }
            });

            request.getQuery().setCombining("Or");

            if (start != null) {
                request.getQuery().setStart(start);
            }

            var data = this.post("Users.get", request);

            Gson gson = new GsonBuilder().create();
            List<User> user = gson.fromJson(data.get("list").toString(), new TypeToken<List<User>>() {
            }.getType());

            return user;

        } catch (JSONRPC2SessionException | KerioClientErrorResponse e) {
            e.printStackTrace();
        }
        return new LinkedList<>();
    }

    public int userCount() {
        var request = new FindUserRequest();
        try {
            request.getQuery().setLimit(1);

            var response = this.post("Users.get", request);
            return ((Long) response.get("totalItems")).intValue();
        } catch (JSONRPC2SessionException | KerioClientErrorResponse e) {
            e.printStackTrace();
        }
        return Integer.MIN_VALUE;
    }

    public List<User> getAllUsers() {
        return getAllUsers(null, null);
    }

    public List<User> getAllUsers(Integer start, Integer limit) {
        var request = new FindAllUserRequest();
        try {
            if (start != null) {
                request.getQuery().setStart(start);
            }
            if (limit != null) {
                request.getQuery().setLimit(limit);
            }
            JSONObject data = this.post("Users.get", request);
            var gson = new GsonBuilder().create();
//            List<User> users = gson.fromJson(data.get("list").toString(), new TypeToken<List<User>>() {}.getType());
            List<User> users = gson.fromJson(data.get("list").toString(), new TypeToken<List<User>>() {
            }.getType());

            return users;

        } catch (JSONRPC2SessionException | KerioClientErrorResponse e) {
            e.printStackTrace();
            return new LinkedList<User>();
        }

    }

    public void updateUser(User user) {
        var request = new UpdateUserRequest();
        request.getUserIds().add(user.getId());
        request.setDetails(user);
        try {
            this.post("Users.set", request);
            var ct = this.getConfigTimestamp();
            this.confirmConfig(ct);
        } catch (JSONRPC2SessionException | KerioClientErrorResponse e) {
            e.printStackTrace();
        }
    }

    protected JSONObject post(String method, Object body) throws JSONRPC2SessionException, KerioClientErrorResponse {
        JSONObject response = null;
        try {
            response = this.sendJsonRpc2Request(method, body);
        } catch (KerioClientErrorResponse kerioClientErrorResponse) {
            // -32001 is "user is not logged in"
            System.out.println("login after first try");
            if (kerioClientErrorResponse.getKerioErrorCode() == -32001) {
                // try once more after a new login
                this.login();
                response = this.sendJsonRpc2Request(method, body);
            } else {
                throw kerioClientErrorResponse;
            }
        }

        return response;
    }

    public Collection<Object> findGroupMembers(GroupModel group, int firstResult, int maxResults) {
        return null;
    }

    public Collection<Group> findGroupByString(String search, Integer start, Integer limit) {
        var request = new FindGroupRequest();
        try {
            request.getQuery().setLimit(1);


            var filterFullName = new FilterCondition("name", FilterComparator.Like, search);
            request.getQuery().getConditions().add(filterFullName);

            if (start != null) {
                request.getQuery().setStart(start);
            }
            if (limit != null) {
                request.getQuery().setLimit(limit);
            }

            var data = this.post("UserGroups.get", request);

            Gson gson = new GsonBuilder().create();
            List<Group> groups = gson.fromJson(data.get("list").toString(), new TypeToken<List<Group>>() {
            }.getType());

            return groups;

        } catch (JSONRPC2SessionException | KerioClientErrorResponse e) {
            e.printStackTrace();
        }
        return new LinkedList<>();
    }
}
