package rocks.homescreen.keycloakuserstorekerio.keriohttpclient;

import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;
import org.aeonbits.owner.ConfigFactory;
import rocks.homescreen.keycloakuserstorekerio.exceptions.KerioClientErrorResponse;

import java.io.IOException;

public class Invoker {
    public static void main(String[] args) throws IOException, JSONRPC2SessionException, KerioClientErrorResponse {
        var invoker = new Invoker();
        invoker.run();
    }

    public void run() throws IOException, JSONRPC2SessionException, KerioClientErrorResponse {
        var cfg = ConfigFactory.create(KerioConfig.class);

//        var client = new KerioUserHttpClient(cfg.kerioUrl());
//
//        System.out.println(client.checkUserAccount(cfg.adminUser(), cfg.adminPassword()));

        var client2 = new KerioHttpClient(
                cfg.kerioUrl(),
                "Kerio APIs Client Library for Java",
                cfg.adminUser(),
                cfg.adminPassword(),
                cfg.kerioDefaultGroup());

//        client2.login();

//        System.out.println(client2.userCount());

//        System.out.println(client2.getAllUsers());
//
//        System.out.println(client2.findUserById("b1e99a54-bc7b-9e45-a0e4-f673d4cdf74e"));
//        System.out.println(client2.findUserByUsername("tilli14@msn.com"));
//        System.out.println(client2.findUserByEmail("tilli14@msn.com"));
//        var ct = client2.getConfigTimestamp();
//        client2.confirmConfig(ct);

//        var response = client2.createUser("hallo7@web.de");
//        var user = response.get();
//        user.setFullName("Hallo7");
//        client2.updateUser(user);
        var group = client2.findGroupByString("Clanwars", 0, 1).stream().findFirst().get();

        var response = client2.getAllUsers();

        response.forEach(user -> {
            if (!user.IsInGroup("Clanwars")) {
                System.out.println(user);
                System.out.println(user.getFullName() + " is not in clanwars group");
                user.getGroups().add(group);
                client2.updateUser(user);
            }
        });


//        System.out.println(response);
//        var ct2 = client2.getConfigTimestamp();
//        client2.confirmConfig(ct2);

//        client2.logout();
    }
}
