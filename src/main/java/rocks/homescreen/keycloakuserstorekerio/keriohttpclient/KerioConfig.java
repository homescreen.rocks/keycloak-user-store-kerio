package rocks.homescreen.keycloakuserstorekerio.keriohttpclient;

import org.aeonbits.owner.Config;

@Config.Sources({
        "file:KerioConfig.properties"
})
public interface KerioConfig extends Config {
    String kerioUrl();

    @DefaultValue("admin")
    String adminUser();

    String adminPassword();

    @DefaultValue("")
    String kerioDefaultGroup();
}
