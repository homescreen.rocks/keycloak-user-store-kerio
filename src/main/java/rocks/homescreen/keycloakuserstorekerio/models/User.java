package rocks.homescreen.keycloakuserstorekerio.models;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class User {
    private UUID id;
    private String email;
    private String fullName;
    private UserCredentials credentials = new UserCredentials();
    private List<Group> groups = new ArrayList<>();

    public boolean IsInGroup(String group){
        return this.groups.stream().anyMatch(group1 -> group1.getName().equals(group));
    }
}
