package rocks.homescreen.keycloakuserstorekerio.models.requests;

import lombok.Data;

@Data
public class UserOrderByClause {
    private String ColumnName = "loginName";
    private String Direction = "Asc";
}
