package rocks.homescreen.keycloakuserstorekerio.models.requests;

import lombok.Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Data
public class GroupQuery {
    private int Start = 0;
    private int Limit = -1;

    private List<GroupOrderByClause> OrderBy = new ArrayList<>(
            Collections.singletonList(new GroupOrderByClause())
    );
    private List<FilterCondition> Conditions = new ArrayList<>();
    private String Combining;
}
