package rocks.homescreen.keycloakuserstorekerio.models.requests;

import lombok.Data;

@Data
public class FilterCondition {
    public FilterCondition(String fieldName, FilterComparator comparator, String value) {
        FieldName = fieldName;
        Comparator = comparator;
        Value = value;
    }

    private String FieldName;
    private FilterComparator Comparator;
    private String Value;
}
