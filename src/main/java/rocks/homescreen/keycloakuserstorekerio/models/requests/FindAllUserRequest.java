package rocks.homescreen.keycloakuserstorekerio.models.requests;

import lombok.Data;

@Data
public class FindAllUserRequest {
    private String DomainId = "local";
    private UserQueryAll Query = new UserQueryAll();
}
