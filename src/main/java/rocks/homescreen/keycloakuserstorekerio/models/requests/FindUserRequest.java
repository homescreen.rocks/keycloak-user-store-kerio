package rocks.homescreen.keycloakuserstorekerio.models.requests;

import lombok.Data;

@Data
public class FindUserRequest {
    private String DomainId = "local";
    private UserQuery Query = new UserQuery();
}
