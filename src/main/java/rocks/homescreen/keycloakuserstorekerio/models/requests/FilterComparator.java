package rocks.homescreen.keycloakuserstorekerio.models.requests;

public enum FilterComparator {
    Eq,
    Like
}
