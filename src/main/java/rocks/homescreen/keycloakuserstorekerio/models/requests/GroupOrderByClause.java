package rocks.homescreen.keycloakuserstorekerio.models.requests;

import lombok.Data;

@Data
public class GroupOrderByClause {
    private String ColumnName = "name";
    private String Direction = "Asc";
}
