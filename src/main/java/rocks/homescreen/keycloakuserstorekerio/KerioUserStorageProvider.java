package rocks.homescreen.keycloakuserstorekerio;

import org.keycloak.component.ComponentModel;
import org.keycloak.credential.CredentialInput;
import org.keycloak.credential.CredentialInputUpdater;
import org.keycloak.credential.CredentialInputValidator;
import org.keycloak.models.*;
import org.keycloak.models.cache.CachedUserModel;
import org.keycloak.models.credential.PasswordCredentialModel;
import org.keycloak.storage.StorageId;
import org.keycloak.storage.UserStorageProvider;
import org.keycloak.storage.user.UserLookupProvider;
import org.keycloak.storage.user.UserQueryProvider;
import org.keycloak.storage.user.UserRegistrationProvider;
import rocks.homescreen.keycloakuserstorekerio.keriohttpclient.KerioHttpClient;
import rocks.homescreen.keycloakuserstorekerio.keriohttpclient.KerioUserHttpClient;
import rocks.homescreen.keycloakuserstorekerio.models.User;
import rocks.homescreen.keycloakuserstorekerio.representations.UserRepresentation;

import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class KerioUserStorageProvider implements UserStorageProvider,
        UserLookupProvider,
        UserRegistrationProvider,
        UserQueryProvider,
        CredentialInputUpdater,
        CredentialInputValidator {

    private final KeycloakSession session;
    private final ComponentModel model;
    private final KerioHttpClient httpClient;
    private final KerioUserHttpClient userHttpClient;
    Logger logger = Logger.getLogger(this.getClass().getName());

    public KerioUserStorageProvider(KeycloakSession session, ComponentModel model, KerioHttpClient kerioHttpClient, KerioUserHttpClient kerioUserHttpClient) {
        this.session = session;
        this.model = model;
        this.httpClient = kerioHttpClient;
        this.userHttpClient = kerioUserHttpClient;
    }

    @Override
    public void close() {
    }

    @Override
    public boolean isConfiguredFor(RealmModel realm, UserModel user, String credentialType) {
        logger.info("isConfiguredFor(" + realm + ", " + user + ", " + credentialType + ")");
        return supportsCredentialType(credentialType);
    }

    @Override
    public boolean isValid(RealmModel realm, UserModel user, CredentialInput credentialInput) {
        logger.info("isValid(" + realm + ", " + user + ", " + credentialInput + ")");
        if (!(credentialInput instanceof UserCredentialModel)) return false;
        if (supportsCredentialType(credentialInput.getType())) {
            try {
                var result = this.userHttpClient.checkUserAccount(user.getUsername(), credentialInput.getChallengeResponse());
                if (result) {
                    logger.info("user logged in: " + user.getUsername());
                } else {
                    logger.info("user tried to loggin with wrong password: " + user.getUsername());
                }
                return result;
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            logger.info("user tried to login with unsupported credential type: " + user.getUsername() + " (" + credentialInput.getType() + ")");
            return false; // invalid cred type
        }
    }

    @Override
    public boolean supportsCredentialType(String credentialType) {
        logger.info("supportsCredentialType(" + credentialType + ")");
        return PasswordCredentialModel.TYPE.equals(credentialType);
    }

    @Override
    public boolean updateCredential(RealmModel realm, UserModel userModel, CredentialInput input) {
        logger.info("updateCredential(" + realm + ", " + userModel + ", " + input + ")");
        if (!supportsCredentialType(input.getType()) || !(input instanceof UserCredentialModel)) return false;
        var kerioUserId = StorageId.externalId(userModel.getId());
        var u = httpClient.findUserById(kerioUserId);
        if (u.isEmpty()) {
            logger.warning("Tried to update user which is not present in this user storage: " + kerioUserId);
            return false;
        }
        var user = u.get();
        user.getCredentials().setUserName(userModel.getUsername());
        user.setEmail(userModel.getEmail());
        if (userModel.getLastName() != null && !userModel.getLastName().trim().isEmpty() && userModel.getFirstName() != null && !userModel.getFirstName().trim().isEmpty()) {
            user.setFullName(userModel.getLastName() + ", " + userModel.getFirstName());
        }
        if (!input.getChallengeResponse().isEmpty()) {
            user.getCredentials().setPassword(input.getChallengeResponse());
            user.getCredentials().setPasswordChanged(true);
        } else {
            user.getCredentials().setPasswordChanged(false);
        }
        httpClient.updateUser(user);
        return true;
    }

    @Override
    public void disableCredentialType(RealmModel realm, UserModel user, String credentialType) {
        logger.info("disableCredentialType(" + realm + ", " + user + ", " + credentialType + ")");
        if (!supportsCredentialType(credentialType)) return;
        getUserRepresentation(user).setPassword(null);
    }

    @Override
    public Set<String> getDisableableCredentialTypes(RealmModel realm, UserModel user) {
        if (getUserRepresentation(user).getPassword() != null) {
            Set<String> set = new HashSet<>();
            set.add(PasswordCredentialModel.TYPE);
            return set;
        } else {
            return Collections.emptySet();
        }
    }

    public UserRepresentation getUserRepresentation(UserModel user) {
        UserRepresentation userRepresentation = null;
        if (user instanceof CachedUserModel) {
            userRepresentation = (UserRepresentation) ((CachedUserModel) user).getDelegateForUpdate();
        } else {
            userRepresentation = (UserRepresentation) user;
        }
        return userRepresentation;
    }

    public UserRepresentation getUserRepresentation(User user, RealmModel realm) {
        return new UserRepresentation(session, realm, model, user);
    }

    @Override
    public int getUsersCount(RealmModel realm) {
        logger.info("getUsersCount(" + realm + ")");
        return httpClient.userCount();
    }

    @Override
    public List<UserModel> getUsers(RealmModel realm) {
        logger.info("getUsers(" + realm + ")");
        return httpClient.getAllUsers()
                .stream()
                .map(user -> new UserRepresentation(session, realm, model, user))
                .collect(Collectors.toList());
    }

    @Override
    public List<UserModel> getUsers(RealmModel realm, int firstResult, int maxResults) {
        logger.info("getUsers(RealmModel realm, int firstResult, int maxResults)");
        return httpClient.getAllUsers(firstResult, maxResults)
                .stream()
                .map(user -> new UserRepresentation(session, realm, model, user))
                .collect(Collectors.toList());
    }

    @Override
    public List<UserModel> searchForUser(String search, RealmModel realm) {
        logger.info("searchForUser(String search, RealmModel realm)");
        var params = new HashMap<String, String>();
        params.put("email", search);
        params.put("username", search);
        params.put("lastName", search); // searches complete fullName
        return httpClient.findUsersByParams(params, 0, -1)
                .stream()
                .map(user -> new UserRepresentation(session, realm, model, user))
                .collect(Collectors.toList());
    }

    @Override
    public List<UserModel> searchForUser(String search, RealmModel realm, int firstResult, int maxResults) {
        logger.info("searchForUser(String search, RealmModel realm, int firstResult, int maxResults)");
        var params = new HashMap<String, String>();
        params.put("email", search);
        params.put("username", search);
        params.put("lastName", search); // searches complete fullName
        return httpClient.findUsersByParams(params, firstResult, maxResults)
                .stream()
                .map(user -> new UserRepresentation(session, realm, model, user))
                .collect(Collectors.toList());
    }

    @Override
    public List<UserModel> searchForUser(Map<String, String> params, RealmModel realm) {
        logger.info("searchForUser(Map<String, String> params, RealmModel realm)");
        return searchForUser(params, realm, 0, 100);
    }

    @Override
    public List<UserModel> searchForUser(Map<String, String> params, RealmModel realm, int firstResult,
                                         int maxResults) {
        logger.info("searchForUser(Map<String, String> params, RealmModel realm, int firstResult,\n" +
                "                                         int maxResults)");
        logger.info("got search request params: " + params.toString());

        return httpClient.findUsersByParams(params, firstResult, maxResults)
                .stream()
                .map(user -> new UserRepresentation(session, realm, model, user))
                .collect(Collectors.toList());
    }

    @Override
    public List<UserModel> getGroupMembers(RealmModel realm, GroupModel group, int firstResult, int maxResults) {
        // TODO Will probably never implement
        logger.info("getGroupMembers(RealmModel realm, GroupModel group, int firstResult, int maxResults)");
        return new ArrayList<>();
    }

    @Override
    public List<UserModel> getGroupMembers(RealmModel realm, GroupModel group) {
        // TODO Will probably never implement
        logger.info("getGroupMembers(RealmModel realm, GroupModel group)");
        return new ArrayList<>();
    }

    @Override
    public List<UserModel> searchForUserByUserAttribute(String attrName, String attrValue, RealmModel realm) {
        // TODO Will probably never implement
        return new ArrayList<>();
    }

    @Override
    public UserModel getUserById(String keycloakId, RealmModel realm) {
        logger.info("getUserById(String keycloakId, RealmModel realm)");
        // keycloakId := keycloak internal id; needs to be mapped to external id
        String id = StorageId.externalId(keycloakId);
        System.out.println(id);
        return new UserRepresentation(session, realm, model, httpClient.findUserById(id).get());
    }

    @Override
    public UserModel getUserByUsername(String username, RealmModel realm) {
        logger.info("getUserByUsername(String username, RealmModel realm)");
        Optional<User> optionalUser = httpClient.findUserByUsername(username);
        return optionalUser.map(user -> getUserRepresentation(user, realm)).orElse(null);
    }

    @Override
    public UserModel getUserByEmail(String email, RealmModel realm) {
        logger.info("getUserByEmail(String email, RealmModel realm)");
        Optional<User> optionalUser = httpClient.findUserByEmail(email);
        return optionalUser.map(user -> getUserRepresentation(user, realm)).orElse(null);
    }

    @Override
    public UserModel addUser(RealmModel realm, String username) {
        logger.info("addUser");
        var user = httpClient.createUser(username);

        return user.map(value -> new UserRepresentation(session, realm, model, value)).orElse(null);
    }

    @Override
    public boolean removeUser(RealmModel realm, UserModel user) {
        logger.info("removeUser(" + realm + ", " + user + ")");
        var uid = StorageId.externalId(user.getId());
        var userEntity = httpClient.findUserById(uid);
        if (userEntity.isEmpty()) {
            logger.info("Tried to delete invalid user with ID " + user.getId());
            return false;
        }
        logger.warning("Deletion of users from Kerio is not supported yet (User ID " + user.getId() + ").");
        return true;
    }
}
