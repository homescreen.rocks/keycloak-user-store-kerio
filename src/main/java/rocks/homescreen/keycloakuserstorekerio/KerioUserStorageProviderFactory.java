package rocks.homescreen.keycloakuserstorekerio;

import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;
import lombok.extern.jbosslog.JBossLog;
import org.apache.commons.lang3.StringUtils;
import org.keycloak.common.util.MultivaluedHashMap;
import org.keycloak.component.ComponentModel;
import org.keycloak.component.ComponentValidationException;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.provider.ProviderConfigurationBuilder;
import org.keycloak.storage.UserStorageProviderFactory;
import rocks.homescreen.keycloakuserstorekerio.exceptions.KerioClientErrorResponse;
import rocks.homescreen.keycloakuserstorekerio.keriohttpclient.KerioHttpClient;
import rocks.homescreen.keycloakuserstorekerio.keriohttpclient.KerioUserHttpClient;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * DemoUserStorageProviderFactory
 */
@JBossLog
public class KerioUserStorageProviderFactory implements UserStorageProviderFactory<KerioUserStorageProvider> {

    protected static final List<ProviderConfigProperty> configMetadata;

    public static final String KERIO_URL = "kerio:url";
    public static final String KERIO_ADMIN_USER = "kerio:adminUser";
    public static final String KERIO_ADMIN_PASSWORD = "kerio:adminPassword";
    public static final String KERIO_DEFAULT_GROUP = "kerio:defaultGroup";

    static {
        configMetadata = ProviderConfigurationBuilder.create()

                .property().name(KERIO_URL)
                .type(ProviderConfigProperty.STRING_TYPE)
                .label("Kerio URL")
                .defaultValue("https://kerio.example.com:4081")
                .add()


                .property().name(KERIO_ADMIN_USER)
                .type(ProviderConfigProperty.STRING_TYPE)
                .label("Kerio Admin User")
                .defaultValue("admin")
                .add()

                .property().name(KERIO_ADMIN_PASSWORD)
                .type(ProviderConfigProperty.PASSWORD)
                .label("Kerio Admin Password")
                .add()

                .property().name(KERIO_DEFAULT_GROUP)
                .type(ProviderConfigProperty.STRING_TYPE)
                .label("Kerio Default User Group")
                .defaultValue(null)
                .add()

                .build();
    }

    private KerioHttpClient httpClient;
    private KerioUserHttpClient userHttpClient;

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return configMetadata;
    }

    @Override
    public KerioUserStorageProvider create(KeycloakSession session, ComponentModel model) {

        if (this.httpClient == null) {
            String kerioUrl = model.getConfig().getFirst(KERIO_URL);
            try {
                this.httpClient = new KerioHttpClient(
                        kerioUrl,
                        "Keycloak Kerio Client",
                        model.getConfig().getFirst(KERIO_ADMIN_USER),
                        model.getConfig().getFirst(KERIO_ADMIN_PASSWORD),
                        model.getConfig().getFirst(KERIO_DEFAULT_GROUP)
                );
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        if (this.userHttpClient == null) {
            String kerioUrl = model.getConfig().getFirst(KERIO_URL);
            this.userHttpClient = new KerioUserHttpClient(kerioUrl);
        }

        return new KerioUserStorageProvider(session, model, this.httpClient, this.userHttpClient);
    }

    @Override
    public void onUpdate(KeycloakSession session, RealmModel realm, ComponentModel oldModel, ComponentModel newModel) {
        onCreate(session, realm, newModel);
    }

    @Override
    public void validateConfiguration(KeycloakSession session, RealmModel realm, ComponentModel config) throws ComponentValidationException {
        MultivaluedHashMap<String, String> configMap = config.getConfig();
        if (StringUtils.isBlank(configMap.getFirst(KERIO_URL))) {
            throw new ComponentValidationException("Kerio URL empty.");
        }
        try {
            new URL(configMap.getFirst(KERIO_URL));
        } catch (MalformedURLException e) {
            throw new ComponentValidationException("Kerio URL is not a valid URL.");
        }
        if (StringUtils.isBlank(configMap.getFirst(KERIO_ADMIN_USER))) {
            throw new ComponentValidationException("Kerio Admin user empty.");
        }
        if (StringUtils.isBlank(configMap.getFirst(KERIO_ADMIN_PASSWORD))) {
            throw new ComponentValidationException("Kerio Admin password empty.");
        }
        try {
            var c = new KerioHttpClient(
                    configMap.getFirst(KERIO_URL),
                    "Keycloak admin login test",
                    configMap.getFirst(KERIO_ADMIN_USER),
                    configMap.getFirst(KERIO_ADMIN_PASSWORD),
                    configMap.getFirst(KERIO_DEFAULT_GROUP)
            );
            c.login();
            c.logout();
        } catch (JSONRPC2SessionException | IOException | KerioClientErrorResponse e) {
            throw new ComponentValidationException("Kerio connection could not be established: " + e.getMessage());
        }
    }

    @Override
    public String getId() {
        return "kerio-users";
    }
}